//
//  Helper.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 14.03.2023.
//

import UIKit

 class BackBarButton: UIButton {
     
//custom leftBarButton
}

extension Date {
    var toString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("EEEE") // "EEEE, dd MM yyyy"
        return dateFormatter.string(from: self)
    }
}
