//
//  GetMethodsFromFile.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 22.02.2023.
//

import UIKit

final class RestoreItemsFromFile {
    let folderName = "WarehouseItemsFolder"
    let fileName = "WarehouseItemFile"
    
    var arrayOfItems:Data?
    
    func getItems() -> [ModelOfItemForSave]? {
        var array: [ModelOfItemForSave]?
        
        let manager = FileManager.default
        
        guard let url = manager.urls(for: .documentDirectory,
                                     in: .userDomainMask).first else {return nil}
        
        let folderURL = url.appendingPathComponent(folderName)
        let fileURL = folderURL.appendingPathComponent(fileName + ".json")
        
        do {
            let data = try Data(contentsOf: fileURL)
            array = try JSONDecoder().decode( [ModelOfItemForSave].self, from: data )
        } catch {
            print(error)
        }
        return array
    }
    
    func createFile() {
        let  manager = FileManager.default
        
        guard let url = manager.urls(for: .documentDirectory,
                                     in: .userDomainMask).first else { return }
        
        let folderUrl = url.appendingPathComponent(folderName)
        
        do {
            try manager.createDirectory(at: folderUrl,
                                        withIntermediateDirectories: true)
            print(folderUrl)
        } catch {
            print(error)
        }
        
        let newItemsCollection:[ModelOfItemForSave] = []
        
        do {
            arrayOfItems = try JSONEncoder().encode(newItemsCollection)
            
        } catch {
            print(error.localizedDescription)
        }
        
        let fileUrl = folderUrl.appendingPathComponent( fileName + ".json")
        manager.createFile(atPath: fileUrl.path,
                           contents: arrayOfItems)
    }
}

