//
//  Controllers.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 30.01.2023.
//

import UIKit
///Определяет какое сообщение вывести в  UIAlertController при добавлении нового item в warehouseData  после проверки на соответствие
final class CheckFields {
        
    private let arrayOfTextEng:[String] = [ "Fill all fields correctly",
                                            "Get name your item",
                                            "Set amount using numbers",
                                            "Item was added",
                                            "This item already exists"]
        
    ///Проверяет данные на возможность конвертировать строку в число, если получаем Int > 0, то возвращает true, в остальном false
    private func checkInt(number:String?) -> Bool {
        if ((Int(number ?? "0") != nil) && Int(number ?? "0")! > 0) {
            return true
        }
        return false
    }
    
    /// Проверяет данные на nil
    private func checkString(text:String) -> Bool {
         if text.isEmpty {
             return false
         }
         return true
     }
    /// Проверяет есть ли такой элемент. Если есть  - возвращает true
    private func doesExist(nameItem:String) -> Bool {
        var trueOrFalse = false
        let arrayOfItems =  WarehouseItems.warehouseItem.itemsArray
        
        for item in arrayOfItems {
            if item.nameOfItem == nameItem {
                trueOrFalse = true
            }
        }
        return trueOrFalse
    }
    
    /// Возвращает текст, который будет использован в UIAlertController. Если все поля заполнены верно, элемент сохранится
    func whatMessageShowAndSave(nameField:String, countField:String) -> (String, UIColor) {
        let name = checkString(text: nameField)
        let count = checkInt(number: countField)
        
        switch (name, count) {
        case (false, false):
            return (arrayOfTextEng[0], .yellow) //"Fill all fields correctly",
            
        case (false, true):
            return (arrayOfTextEng[1], .yellow) //"Get name your item",
            
        case (true, false):
            return (arrayOfTextEng[2], .yellow) //"Set amount using numbers",
            
        case(true, true):
            if doesExist(nameItem:nameField) {
                return (arrayOfTextEng[4], .red)// "This item already exists"
            } else {
                return (arrayOfTextEng[3], .green) // "Item was added"
            }
        }
    }
}
