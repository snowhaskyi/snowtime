//
//  DeleteMethods.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 01.03.2023.
//

import UIKit

final class DeleteMethods {
    
    let folderName = "WarehouseItemsFolder"
    let fileName = "WarehouseItemFile"
    
    func deleteItem(index: Int) {
        let  manager = FileManager.default
        
        guard let url = manager.urls(for: .documentDirectory,
                                     in: .userDomainMask).first else { return }

        let folderUrl = url.appendingPathComponent(folderName)
        
        var arrayOfItems:Data?

        var array = RestoreItemsFromFile().getItems()
        
        if array?[index].sourceImage == "photo" {
            
            guard let urlImage = URL(string: (array?[index].imageURL) ?? "nil") else { return }
            do {
                print(urlImage)
                try manager.removeItem(at: urlImage)
            } catch {
                print(error)
            }
        }

        array?.remove(at: index)
        
        do {
            arrayOfItems = try JSONEncoder().encode(array)

        } catch {
            print(error.localizedDescription)
        }
        
        let fileUrl = folderUrl.appendingPathComponent( fileName + ".json")
        manager.createFile(atPath: fileUrl.path,
                           contents: arrayOfItems)
    }
}
