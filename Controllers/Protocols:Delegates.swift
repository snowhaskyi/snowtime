//
//  Protocols:Delegates.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 01.02.2023.
//

import UIKit

protocol ReloadCollectionView {
    func reloadCollectionView()
}


protocol SetNewImage {
    func setImage(name:String)
}
