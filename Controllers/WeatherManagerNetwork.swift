//
//  WeatherManagerNetwork.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 18.03.2023.
//

import Foundation

class WeatherManagerNetWork {
    
    private  let myWeatherAPIKey: String = "1db1567a-3b57-4812-9709-62e33ee9bd35"
    private  let yandexApiKey: String = "X-Yandex-API-Key"
    
    func requestWeatherForLocation(lat:Double, long: Double) {
        let urlString = "https://api.weather.yandex.ru/v2/forecast?lat=\(lat)&lon=\(long)&lang=ru_RU&limit=7&hours=true&extra=true"
                
        guard let url = URL(string: urlString) else { return }
        
        var request = URLRequest(url: url, timeoutInterval: Double.infinity)
        request.addValue("1db1567a-3b57-4812-9709-62e33ee9bd35", forHTTPHeaderField: "X-Yandex-API-Key")
        request.httpMethod = "GET"
        
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else { return }
         //   print(String(data:data, encoding: .utf8)!)
            do {
                let weatherData = try JSONDecoder().decode(WeatherJSON.self, from: data)                
                WeatherModel.weather.dayToday = weatherData.now
                WeatherModel.weather.city = weatherData.geo_object.locality.name
                WeatherModel.weather.temp = weatherData.fact.temp
                WeatherModel.weather.overcast = weatherData.fact.condition
                WeatherModel.weather.windSpeed = weatherData.fact.wind_speed
                WeatherModel.weather.windGust = weatherData.fact.wind_gust
                
                //need to remove current day, but i cant use "dropFirst".. so i used this miracle method, sorry)
                WeatherModel.weather.daysArray = weatherData.forecasts.reversed().dropLast().reversed()
            } catch {
                print(error)
            }
        }
        task.resume()
    }
}
