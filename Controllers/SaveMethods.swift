//
//  SaveMethods.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 31.01.2023.
//

import UIKit

final class SaveMethods {
    let folderName = "WarehouseItemsFolder"
    let fileName = "WarehouseItemFile"
    
    var arrayOfItems:Data?

    func saveItemWithPhoto(image: UIImage,
                           imageName: String,
                           name:String,
                           count: Int,
                           sourceImage:String){
    
        let  manager = FileManager.default
        
        guard let url = manager.urls(for: .documentDirectory,
                                     in: .userDomainMask).first else { return }
        
        let folderUrl = url.appendingPathComponent(folderName)
        
        //Save image if it's make by photo
        var urlForImage:String? = nil
        
        if TypeOfGallerySource.TOGS.state == .photo {
            let newName = transliterate(nonLatin: imageName)
            let imageUrl = folderUrl.appendingPathComponent( newName + ".jpeg")
            let data: NSData = image.jpegData(compressionQuality: 1)! as NSData
            let newImage = UIImage(data: data as Data)
            let imageData = newImage?.jpegData(compressionQuality: 1.0)
            
            manager.createFile(atPath: imageUrl.path,
                               contents: imageData)
            
            urlForImage = "file://" + imageUrl.path

        } else {
            urlForImage = TypeOfGallerySource.TOGS.name
        }
        
        do {
            try manager.createDirectory(at: folderUrl,
                                        withIntermediateDirectories: true)
            print(folderUrl)
        } catch {
            print(error)
        }
    
        let savingItem = ModelOfItemForSave(nameOfItem: name,
                                            count: count,
                                            imageUrl: urlForImage ?? "none",
                                            sourceImage: sourceImage)
        
        var newItemsCollection = WarehouseItems.warehouseItem.itemsArray
        newItemsCollection.append(savingItem)
        
        do {
            arrayOfItems = try JSONEncoder().encode(newItemsCollection)

        } catch {
            print(error.localizedDescription)
        }
        
        let fileUrl = folderUrl.appendingPathComponent( fileName + ".json")
        manager.createFile(atPath: fileUrl.path,
                           contents: arrayOfItems)
    }
}

 final class TypeOfGallerySource {
    static var TOGS = TypeOfGallerySource()
    
    var name: String? = nil
     var state: TypeSource = .none
    
    init(name: String? = nil,
         imageURL:String? = nil) {
        
        self.name = name
        
    }
    
     enum TypeSource: String {
         case gallery = "gallery"
         case photo = "photo"
         case none = "none"
    }
}

extension SaveMethods {
   private func transliterate(nonLatin: String) -> String {
        return nonLatin
            .applyingTransform(.toLatin, reverse: false)?
            .applyingTransform(.stripDiacritics, reverse: false)?
            .lowercased()
            .replacingOccurrences(of: " ", with: "-") ?? nonLatin
    }
}
