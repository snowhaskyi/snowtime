//
//  SceneDelegate.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 22.01.2023.

import UIKit
import CoreLocation

final class MainView: UIViewController, CLLocationManagerDelegate {
    
    let warehouse = ButtonForMainView()
    let work = ButtonForMainView()
    let projects = ButtonForMainView()
    let weather = ButtonForMainView()
    let button = ButtonForMainView()
    
    var item = WarehouseItems().itemsArray
        
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

        let elements = [warehouse, projects, work, weather,]
        for i in elements {
            view.addSubview(i)
            i.translatesAutoresizingMaskIntoConstraints = false
        }
        custom()
        constraints()
        background()
        
        if item.isEmpty {
            RestoreItemsFromFile().createFile()
        }
    }
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?

        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            setupLocation()
        }
        
        private  func setupLocation() {
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
            
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
            let weatherManagerNetWork = WeatherManagerNetWork()
    
            if !locations.isEmpty, currentLocation == nil {
                currentLocation = locations.first
                locationManager.stopUpdatingLocation()
                guard let currentLoc = currentLocation else { return }
                
                let longitude = currentLoc.coordinate.longitude
                let latitude = currentLoc.coordinate.latitude

                weatherManagerNetWork.requestWeatherForLocation(lat: latitude,
                                                                long: longitude)
            }
        }
    
   private func background() {
        let background = UIImage(named: "StartBackground")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
    }
        
    //MARK: Кнопки перехода
    @objc func goToWarehouseVC() {
        let newVC = WarehouseCollection()
        navigationController?.pushViewController(newVC, animated: true)
    }
    
    @objc func goToWorkVC() {
        let newVC = WorkListCollection()
        navigationController?.pushViewController(newVC, animated: true)

        //showAlert(text: "IMPOSSIBLE!!")
    }
    
    @objc func goToProjectVC() {
        showAlert(text: "IMPOSSIBLE!!")
    }
    
    @objc func goToWeatherVC() {
        let newVC = WeatherMainView()
        navigationController?.pushViewController(newVC, animated: true)
    }
    
    //MARK: Конфигурации отображения
    private func custom() {
        warehouse.configure(with: CustomButtonViewModel(title: "Склад",  imageName: "warehouseIcon"))
        work.configure(with: CustomButtonViewModel(title: "Работы", imageName: "worksIcon"))
        projects.configure(with: CustomButtonViewModel(title: "Шаблоны", imageName: "sketchIcon"))
        weather.configure(with: CustomButtonViewModel(title: "Погода", imageName: "weatherIcon"))
        
        warehouse.layer.borderColor = UIColor.customCurrant.cgColor
        work.layer.borderColor = UIColor.workColorLightBlue.cgColor
        projects.layer.borderColor = UIColor.red.cgColor
        weather.layer.borderColor = UIColor.customLightPink.cgColor

        warehouse.addTarget(self, action:#selector(goToWarehouseVC), for: .touchUpInside)
        work.addTarget(self, action:#selector(goToWorkVC), for: .touchUpInside)
        projects.addTarget(self, action:#selector(goToProjectVC), for: .touchUpInside)
        weather.addTarget(self, action: #selector(goToWeatherVC), for: .touchUpInside)
    }

   private func constraints(){
        let width = view.frame.width
        let height = view.frame.height
        
        let buttonWidth:CGFloat = 300
        let buttonHeight:CGFloat = 80
        let spaceBetweenButton:CGFloat = 30
        
        NSLayoutConstraint.activate([
            warehouse.topAnchor.constraint(equalTo: view.topAnchor, constant: (height-buttonHeight)/3),
            warehouse.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: (width - buttonWidth)/2),
            warehouse.widthAnchor.constraint(equalToConstant:  buttonWidth),
            warehouse.heightAnchor.constraint(equalToConstant: buttonHeight),
            
            work.topAnchor.constraint(equalTo: warehouse.bottomAnchor, constant: spaceBetweenButton),
            work.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: (width - buttonWidth)/2),
            work.widthAnchor.constraint(equalToConstant:  buttonWidth),
            work.heightAnchor.constraint(equalToConstant: buttonHeight),
            
            projects.topAnchor.constraint(equalTo: work.bottomAnchor, constant: spaceBetweenButton),
            projects.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: (width - buttonWidth)/2),
            projects.widthAnchor.constraint(equalToConstant:  buttonWidth),
            projects.heightAnchor.constraint(equalToConstant: buttonHeight),
            
            weather.topAnchor.constraint(equalTo: projects.bottomAnchor, constant: spaceBetweenButton),
            weather.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: (width - buttonWidth)/2),
            weather.widthAnchor.constraint(equalToConstant:  buttonWidth),
            weather.heightAnchor.constraint(equalToConstant: buttonHeight),
        ])
    }
    
  private func showAlert(text:String) {
        let alertControl = UIAlertController(title: nil, message: text, preferredStyle: .alert)
        alertControl.view.backgroundColor = .red
       
        present(alertControl, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            alertControl.dismiss(animated: true, completion: nil)
        }
    }
}

