//
//  WorkListCollection.swift
//  ShowTime-Project
//  Created by Wi-Fai on 22.01.2023.
//

import UIKit

final class WorkListCollection: UIViewController {
    
    var collectionView: UICollectionView!
    //    var works
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backBarButton()
        createCollectionView()
    }    
}

extension WorkListCollection: UICollectionViewDataSource,
                              UICollectionViewDelegate,
                              UICollectionViewDelegateFlowLayout {
    //MARK: CollectionView config
    private func createCollectionView() {
        collectionView = UICollectionView(frame: .zero,
                                          collectionViewLayout: createFlowLayout())
        collectionView.backgroundColor = .workColorLightBlue
        
        collectionView.dataSource = self
        collectionView.delegate = self
                
        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.register(WarehouseCollectionCell.self,
                                forCellWithReuseIdentifier: "\(WarehouseCollectionCell.self)")

        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0)
        ])
    }
    
    private func createFlowLayout()  -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = .init(top: 10, left: 10, bottom: 30, right: 10)
        return layout
    }
  //MARK: CollectionView delegate and DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(WarehouseCollectionCell.self)",
                                                          for: indexPath) as? WarehouseCollectionCell
        return itemCell!
    }
    
    
    // нужно упростить  до вызова класса блэт!
    private func backBarButton() {
        let button = UIButton(type: .custom)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        button.backgroundColor = .clear
        button.setImage(UIImage(named: "backIcon"), for: .normal)
        button.addTarget(self, action: #selector(back), for: UIControl.Event.touchUpInside)

        let menuBarItem = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = menuBarItem
    }
    
    @objc func back() {
       navigationController?.popViewController(animated: true)
   }

}
