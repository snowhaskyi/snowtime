//  Created by Wi-Fai on 22.01.2023.
//

import UIKit

class WorkListCell: UITableViewCell {
    
   static let identifier = "Custom"

    let name = UILabel()
    let data = UILabel()
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        contentView.backgroundColor = .black
        contentView.addSubview(name)
        contentView.addSubview(data)
        name.translatesAutoresizingMaskIntoConstraints = false
        data.translatesAutoresizingMaskIntoConstraints = false

        custom()
        constraint()
        
    }
    
    func custom() {
        name.text = "Название"
        name.textAlignment = .center
        name.backgroundColor = .white
        name.textColor = .white
        name.backgroundColor = .black
        
        data.text = "Дата"
        data.textAlignment = .center
        data.backgroundColor = .white
        data.textColor = .white
        data.backgroundColor = .black
    }
    
    func constraint(){
        NSLayoutConstraint.activate([
            contentView.heightAnchor.constraint(equalToConstant: 70),
            name.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            name.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            name.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
            
            data.topAnchor.constraint(equalTo: name.bottomAnchor, constant: 10),
            data.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            data.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
            data.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
        ])
    }

}
