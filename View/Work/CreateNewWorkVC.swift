//
//  Created by Wi-Fai on 22.01.2023.
//

// Идея черновая, которую надо пересмотреть

/* ЧТО НАДО СДЕЛАТЬ:
 -Название проекта должно выпадать список предложенных вариантов
 -Место работы описать словами и так же поставить точку на карте
 -Время вводить через DataPicker
 -Предлагать прикреплять фото с прошлого такого проекта (если оно повторяется)
 */

import UIKit

final class CreateNewWorkVC: UIViewController {
    
    let name = UITextField()
    let place = UITextField()
    let time = UITextField()
    let project = UITextField()
    let descript = UITextField()
    let addButton = UIButton()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        view.backgroundColor = .black
        
        let array = [name, place, time, project, descript, addButton]
        for i in array {
            view.addSubview(i)
            i.translatesAutoresizingMaskIntoConstraints = false
        }
        
        custom()
        constraint()
    }
    
    func custom() {
        
        let array = [name, place, time, project, descript]
        for item in array {
            item.backgroundColor = .white
            item.textAlignment = .center
            item.tintColor = .black
            item.layer.cornerRadius = 10
            item.layer.borderWidth = 4
            item.layer.borderColor = .init(red: 1, green: 1, blue: 1, alpha: 1)
        }
        
        name.placeholder = "Название"
        place.placeholder = "Место"
        time.placeholder = "Время"
        project.placeholder = "Проект"
        descript.placeholder = "Доп.описание"
        
        addButton.setTitle("Добавить", for: .normal)
        addButton.backgroundColor = .systemBlue
        addButton.layer.cornerRadius = 20
        //addButton.addTarget(self, action:#selector(toAddElementWarehouse), for: .touchUpInside)
    }
    
    func constraint() {
        
        let array = [name, place, time, project,]
        for i in array {
            NSLayoutConstraint.activate([
                i.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                i.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
                i.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
                i.heightAnchor.constraint(equalToConstant: 40),
            ])
        }
        
        NSLayoutConstraint.activate([
            name.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            place.topAnchor.constraint(equalTo: name.bottomAnchor, constant: 40),
            time.topAnchor.constraint(equalTo: place.bottomAnchor, constant: 40),
            project.topAnchor.constraint(equalTo: time.bottomAnchor, constant: 40),
            descript.topAnchor.constraint(equalTo: project.bottomAnchor, constant: 40),
            descript.heightAnchor.constraint(equalToConstant: 100),
            descript.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            descript.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            descript.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            //descript.heightAnchor.constraint(equalToConstant: 40),

            
            addButton.topAnchor.constraint(equalTo: descript.bottomAnchor, constant: 40),
            addButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            addButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            addButton.heightAnchor.constraint(equalToConstant: 50),
        ])
        
    }
    
}
