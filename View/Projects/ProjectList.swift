//  Created by Wi-Fai on 22.01.2023.
//

import UIKit

final class ProjectList: UIViewController, UITableViewDelegate, UITableViewDataSource {
        
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(ProjectListCell.self, forCellReuseIdentifier: ProjectListCell.identifier)
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = .white
        tableView.separatorInset = .init(top: 0, left: 45, bottom: 0, right: 45)
        return tableView
    }()
    
    
    
    let addButton = UIButton()
    
    @objc func toAddWorkVC () {
        let newVC = CreateNewProjectVC()
        navigationController?.pushViewController(newVC, animated: true)
    }
    
    func  constraints() {
        NSLayoutConstraint.activate([
            addButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
            addButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50),
            addButton.widthAnchor.constraint(equalToConstant:  100),
            addButton.heightAnchor.constraint(equalToConstant: 40),
        ])
    }
    
    func custom() {
        addButton.setTitle("Добавить", for: .normal)
        addButton.backgroundColor = .systemBlue
        addButton.layer.cornerRadius = 20
        addButton.addTarget(self, action:#selector(toAddWorkVC), for: .touchUpInside)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Проекты"
        tableView.dataSource = self
        tableView.delegate = self
        view.addSubview(tableView)
        view.addSubview(addButton)
        addButton.translatesAutoresizingMaskIntoConstraints = false
        custom()
        constraints()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.backgroundColor = .black
        tableView.frame = view.bounds
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ProjectListCell.identifier, for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
}
