//  Created by Wi-Fai on 22.01.2023.
//

import UIKit

class ProjectListCell: UITableViewCell {
    
    static let identifier = "projectListCell"
    
    let image = UIImageView()
    let name = UILabel()
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        contentView.backgroundColor = .black
        
        contentView.addSubview(image)
        contentView.addSubview(name)
        image.translatesAutoresizingMaskIntoConstraints = false
        name.translatesAutoresizingMaskIntoConstraints = false
        
        constraint()
        custom()
    }
    
    func constraint() {
       
        NSLayoutConstraint.activate([
            image.heightAnchor.constraint(equalToConstant: 70),
            image.widthAnchor.constraint(equalToConstant: 70),
            image.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            image.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,constant: -10),
            image.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 40),
            
            name.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            name.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: 10),
            name.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
        ])
    }
    
    func custom() {
        image.backgroundColor = .gray
        image.layer.borderColor = .init(red: 1, green: 1, blue: 1, alpha: 1)
        image.layer.borderWidth = 2
        image.layer.cornerRadius = 10

        name.text = "Название"
        name.textAlignment = .center
        name.backgroundColor = .white
        name.textColor = .white
        name.backgroundColor = .black
    }

}
