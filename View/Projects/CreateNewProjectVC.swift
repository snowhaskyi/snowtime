//  Created by Wi-Fai on 22.01.2023.
//Предполагается картинка выводит на экран камеры или загрузка фото из  галереи
// кнопка сохранить переписать в последнюю ячейку  табличного представления

import UIKit

final class CreateNewProjectVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let image = UIImageView()
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(ListCellForProject.self, forCellReuseIdentifier: ListCellForProject.identifier)
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = .white
        tableView.separatorInset = .init(top: 0, left: 45, bottom: 0, right: 45)

        return tableView
    }()
    
    let name = UITextField()
    let saveButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLayoutSubviews()
        
        view.addSubview(image)
        image.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(name)
        name.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(saveButton)
        saveButton.translatesAutoresizingMaskIntoConstraints = false
        
        tableView.dataSource = self
        tableView.delegate = self
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        view.backgroundColor = .black
        constraint()
        custom()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.backgroundColor = .black
        //так не пойдет. Ширину надо подогнать автоматически под ширину девайса
        // отталкиваться от значения  ширины вью
        tableView.frame = CGRect(x: 0, y: 280, width: 390, height: 480)
    }

    
    func constraint() {
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            image.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            image.heightAnchor.constraint(equalToConstant: 120),
            image.widthAnchor.constraint(equalToConstant: 120),
            
            name.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 10),
            name.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            name.heightAnchor.constraint(equalToConstant: 30),
            name.widthAnchor.constraint(equalToConstant: 200),
            
            saveButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:  -30),
            saveButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -30),
            saveButton.heightAnchor.constraint(equalToConstant: 45),
            saveButton.widthAnchor.constraint(equalToConstant: 100),
        ])
    }
    
    func custom(){
        image.backgroundColor = .gray
        image.layer.borderColor = .init(red: 1, green: 1, blue: 1, alpha: 1)
        image.layer.borderWidth = 2
        image.layer.cornerRadius = 10
        
        name.placeholder = "Название проекта"
        name.textAlignment = .center
        name.backgroundColor = .white
        name.layer.borderWidth = 2
        name.layer.cornerRadius = 10
        
        saveButton.tintColor = .white
        saveButton.setTitle("Сохранить", for: .normal)
        saveButton.backgroundColor = .systemBlue
        saveButton.layer.cornerRadius = 20

    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ListCellForProject.identifier, for: indexPath)
        return cell

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }


    
}




