//
//  ListCellForProject.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 22.01.2023.
//

import UIKit

class ListCellForProject: UITableViewCell {
    
    static let identifier = "ListCellForProject"
    
    let image = UIImageView()
    let name = UILabel()
    let number = UITextField()

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        contentView.backgroundColor = .black
        contentView.addSubview(image)
        contentView.addSubview(name)
        contentView.addSubview(number)
        
        image.translatesAutoresizingMaskIntoConstraints = false
        name.translatesAutoresizingMaskIntoConstraints = false
        number.translatesAutoresizingMaskIntoConstraints = false
        custom()
        constraint()
    }
        
    func custom() {
        image.backgroundColor = .gray
        image.layer.borderColor = .init(red: 1, green: 1, blue: 1, alpha: 1)
        image.layer.borderWidth = 2
        image.layer.cornerRadius = 10
        
        name.text = "Название"
        name.textAlignment = .center
        name.backgroundColor = .white
        name.textColor = .white
        name.backgroundColor = .black
        
        number.placeholder = "Количество"
        number.backgroundColor = .white
        number.textAlignment = .center
        number.tintColor = .black
        number.layer.cornerRadius = 10
        number.layer.borderWidth = 4
        number.layer.borderColor = .init(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    
    func constraint() {
        NSLayoutConstraint.activate([
            image.heightAnchor.constraint(equalToConstant: 70),
            image.widthAnchor.constraint(equalToConstant: 70),
            image.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            image.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,constant: -10),
            image.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 40),
            
            name.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            name.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: 20),
            name.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant:  -20),
            name.heightAnchor.constraint(equalToConstant: 20),
            
            number.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            number.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: 50),
            number.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -50),
            number.heightAnchor.constraint(equalToConstant: 25),
        ])
    }
    

}
