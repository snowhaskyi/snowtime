//
//  WarehouseCollection.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 23.01.2023.
//
import UIKit
final class WarehouseCollection: UIViewController,
                                 UICollectionViewDataSource,
                                 ReloadCollectionView,
                                 UICollectionViewDelegate,
                                 UICollectionViewDelegateFlowLayout{
        
    var collectionView: UICollectionView!
    let addItemView = UIButton()
    let findTextField = UITextField()
    let searchBar = UISearchBar()
    let dismissSearchKeyboard = UIButton()
    
    var items: [ModelOfItemForSave] {RestoreItemsFromFile().getItems() ?? []}
    //var items = models
    var filterItems:[ModelOfItemForSave] = []
    var isSearching = false
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        backBarButton()
        createCollectionView()
        addButtonSetup()
        finderField()
        dismissSearchKeyboardButton()
    }
    
    private func finderField() {
        view.addSubview(searchBar)
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.placeholder = "item"
        searchBar.barStyle = .black
        searchBar.searchBarStyle = .default
        searchBar.isTranslucent = false
        
        searchBar.layer.cornerRadius = 5
        searchBar.layer.borderWidth = 2
        searchBar.layer.borderColor = UIColor.white.cgColor
        
        NSLayoutConstraint.activate([
            searchBar.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            searchBar.widthAnchor.constraint(equalToConstant: view.frame.size.width / 1.8),
            searchBar.heightAnchor.constraint(equalToConstant: 40),
            view.keyboardLayoutGuide.topAnchor.constraint(equalToSystemSpacingBelow: searchBar.bottomAnchor,
                                                          multiplier: 4.2),
        ])
    }
    
    private func dismissSearchKeyboardButton() {
        dismissSearchKeyboard.setImage(UIImage(named: "Dismiss"), for: .normal)
        dismissSearchKeyboard.backgroundColor = .clear
        dismissSearchKeyboard.addTarget(self, action: #selector(dismissBut), for: .touchUpInside)
        dismissSearchKeyboard.isHidden = true
        
        view.addSubview(dismissSearchKeyboard)
        dismissSearchKeyboard.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            dismissSearchKeyboard.widthAnchor.constraint(equalToConstant: 30),
            dismissSearchKeyboard.heightAnchor.constraint(equalToConstant: 30),
            dismissSearchKeyboard.trailingAnchor.constraint(equalTo: searchBar.trailingAnchor, constant: 20),
            view.keyboardLayoutGuide.topAnchor.constraint(equalToSystemSpacingBelow: dismissSearchKeyboard.bottomAnchor,
                                                          multiplier: 4.2),
        ])
     }
    
    @objc func dismissBut() {
        searchBar.resignFirstResponder()
        dismissSearchKeyboard.isHidden = true
        searchBar.text = ""
        isSearching = false
        collectionView.reloadData()
    }
    
    private func addButtonSetup() {
        addItemView.setImage(UIImage(named: "addIcon"), for: .normal)
        addItemView.backgroundColor = .clear
        addItemView.addTarget(self, action: #selector(toAddView), for: .touchUpInside)
        
        view.addSubview(addItemView)
        addItemView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            addItemView.widthAnchor.constraint(equalToConstant: 60),
            addItemView.heightAnchor.constraint(equalToConstant: 60),
            addItemView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            addItemView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100),
        ])
    }
    
    private func createCollectionView() {
        collectionView = UICollectionView(frame: .zero,
                                          collectionViewLayout: createFlowLayout())
        collectionView.backgroundColor = .customCurrant
        
        collectionView.dataSource = self
        collectionView.delegate = self
                
        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.register(WarehouseCollectionCell.self,
                                forCellWithReuseIdentifier: "\(WarehouseCollectionCell.self)")

        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0)
        ])
    }
    
    private func createFlowLayout()  -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = .init(top: 10, left: 10, bottom: 30, right: 10)
        return layout
    }
    
    //MARK: CollectionView DataSource/Delegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let alert = UIAlertController(title: nil, message: "DELETE?", preferredStyle: .actionSheet)
        let actionYes = UIAlertAction(title: "Yes", style: .default) {_ in
            DeleteMethods().deleteItem(index: indexPath.row)
            collectionView.reloadData()
        }
        
        let actionNo = UIAlertAction(title: "No", style: .destructive)
        alert.addAction(actionYes)
        alert.addAction(actionNo)
        present(alert, animated: true)
    }
        
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         if isSearching {
            return filterItems.count
         } else {
             return items.count
         }
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         if isSearching {
             let cell =  createCell(array: filterItems, index: indexPath)
             return cell
         } else {
             let cell = createCell(array: items, index: indexPath)
             return cell
         }
    }
    
    @objc func toAddView() {
        let newVC = AddItemSheetViewController()
        newVC.reloadDelegate = self

        if let sheet = newVC.sheetPresentationController {
                 sheet.detents = [.large()]
                 sheet.prefersScrollingExpandsWhenScrolledToEdge = false
                 sheet.prefersGrabberVisible = false
                 sheet.preferredCornerRadius = 22
             }
             present(newVC, animated: true, completion: nil)
    }
}

extension WarehouseCollection: UISearchBarDelegate {
    private func backBarButton() {
        let button = UIButton(type: .custom)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        button.backgroundColor = .clear
        button.setImage(UIImage(named: "backIcon"), for: .normal)
        button.addTarget(self, action: #selector(back), for: UIControl.Event.touchUpInside)

        let menuBarItem = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = menuBarItem
    }
        
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterItems.removeAll()
        dismissSearchKeyboard.isHidden = false
        
        guard searchText != "" || searchText != " " else { return }
        
        for item in items {
            let text = searchText.lowercased()
            let isArrayContain = item.nameOfItem.lowercased().range(of: text)
            
            if isArrayContain != nil {
                filterItems.append(item)
            }
        }
        
        if searchBar.text == "" {
            isSearching = false
            collectionView.reloadData()
        } else {
            isSearching = true
            collectionView.reloadData()
        }
    }

     @objc func back() {
        navigationController?.popViewController(animated: true)
    }
    
     func reloadCollectionView() {
        collectionView.reloadData()
    }
    
    private func createCell(array: [ModelOfItemForSave], index: IndexPath) -> UICollectionViewCell {
        
        let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(WarehouseCollectionCell.self)",
                                                          for: index) as? WarehouseCollectionCell

        itemCell?.count.text = String(array[index.row].count) + " шт"
        itemCell?.name.text = array[index.row].nameOfItem
        
        switch  array[index.row].sourceImage {
        case "gallery":
            itemCell?.imageView.image = UIImage(named:array[index.row].imageURL ?? "emptyImage")
        case "photo":
            if let url = URL(string: array[index.row].imageURL ?? "nil") {
                do {
                    let data = try Data(contentsOf: url)
                    itemCell?.imageView.image = UIImage(data: data)
                } catch {
                    print("Error = ", error.localizedDescription)
                }
            }
        case "none":
            itemCell?.imageView.image = UIImage(named:"Nothing")
        default:
            itemCell?.imageView.image = UIImage(named: "Nothing")
        }
        return itemCell!
    }
}


//let models: [ModelOfItemForSave] = [ ModelOfItemForSave(nameOfItem: "Ferma", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "Arca", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "square", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "hamer", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "Fuck", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "Ass", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "Hand", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "Coworker", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "Dick", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "Book", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "Pen", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "4len", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "Бутылка", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "Зефирка", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "Утка", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "Жвачка", count: 34, imageUrl: "camera", sourceImage: "none"),
//                                     ModelOfItemForSave(nameOfItem: "телефон", count: 34, imageUrl: "camera", sourceImage: "none"),
//]
