//
//  GeneralGallery.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 16.02.2023.
//

import UIKit

final class GeneralGallery: UIViewController,
                            UICollectionViewDelegate,
                            UICollectionViewDataSource  {

    var collectionOfImages: UICollectionView!
    
    var arrayOfImage: [String] = ["Shield", "LaunchImage", "Ferma", "Cube",]
    
    var setImage: SetNewImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        TypeOfGallerySource.TOGS.state = .gallery
        configForCollectionView()
    }
    
    func setupFlowLayout()  -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        layout.scrollDirection = .vertical
        layout.sectionInset = .init(top: 3, left: 10, bottom: 0, right: 10)
        return layout
    }
    
    func configForCollectionView() {
        
        collectionOfImages = UICollectionView(frame: .zero, collectionViewLayout: setupFlowLayout())
        collectionOfImages.backgroundColor = .customDarkPink.withAlphaComponent(0.5)
        collectionOfImages.layer.cornerRadius = 20
        collectionOfImages.backgroundColor = .black
        
        collectionOfImages.dataSource = self
        collectionOfImages.delegate = self
        
        collectionOfImages.isEditing = false
        
        collectionOfImages.register(WarehouseCollectionCell.self,
                                forCellWithReuseIdentifier: "\(WarehouseCollectionCell.self)")

        view.addSubview(collectionOfImages)
        collectionOfImages.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            collectionOfImages.topAnchor.constraint(equalTo: view.topAnchor, constant: 10),
            collectionOfImages.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            collectionOfImages.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            collectionOfImages.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10)
        ])
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        setImage?.setImage(name: arrayOfImage[indexPath.row])
        TypeOfGallerySource.TOGS.state = .gallery
        TypeOfGallerySource.TOGS.name = arrayOfImage[indexPath.row]
    }
    
}

extension GeneralGallery {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrayOfImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(WarehouseCollectionCell.self)",
                                                          for: indexPath) as? WarehouseCollectionCell
        cell?.imageView.image = UIImage(named: arrayOfImage[indexPath.row])
        return cell!
    }
}
