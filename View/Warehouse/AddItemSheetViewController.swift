//
//  AddItemViewController.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 25.01.2023.
//

import UIKit

class AddItemSheetViewController: UIViewController,
                                  UITextFieldDelegate,
                                  UICollectionViewDataSource,
                                  UICollectionViewDelegate,
                                  SetNewImage{
            
    let grabImageOnTop = UIImageView()
    let imageOfItem = UIImageView()
    let nameOfItem = UITextField()
    let countOfItem = UITextField()
    let addButton = UIButton()
    let photoButton = UIButton()
    let selfGalleryButton = UIButton()
    var lastAddedItemCollection: UICollectionView!
    let lastAddedLabel = UILabel()
    //Возвращает последние пять созданных элементов
    var reversedArray: [ModelOfItemForSave] {RestoreItemsFromFile().getItems()?.reversed() ?? [] }
    
    var reloadDelegate: ReloadCollectionView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .customVioletDark
        
        addItemOnView()
        configure()
        constraints()
        configForCollectionView()
    }
    
    func addItemOnView(){
        let arrayItemsOnView = [grabImageOnTop, imageOfItem, nameOfItem, countOfItem, addButton, photoButton, selfGalleryButton, lastAddedLabel]
        for item in arrayItemsOnView {
            view.addSubview(item)
            item.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    func configForCollectionView() {
        lastAddedItemCollection = UICollectionView(frame: .zero, collectionViewLayout: setupFlowLayout())
        if reversedArray.isEmpty {
            lastAddedItemCollection.isHidden = true
        }
        lastAddedItemCollection.backgroundColor = .customDarkPink.withAlphaComponent(0.5)
        lastAddedItemCollection.layer.cornerRadius = 20
        
        lastAddedItemCollection.dataSource = self
        lastAddedItemCollection.delegate = self
        
        lastAddedItemCollection.register(WarehouseCollectionCell.self,
                                forCellWithReuseIdentifier: "\(WarehouseCollectionCell.self)")

        view.addSubview(lastAddedItemCollection)
        lastAddedItemCollection.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            lastAddedItemCollection.topAnchor.constraint(equalTo: addButton.bottomAnchor, constant: 70),
            lastAddedItemCollection.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            lastAddedItemCollection.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            lastAddedItemCollection.heightAnchor.constraint(equalToConstant: 190)
        ])
    }
     //return last five items
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let lastFiveItems = reversedArray.prefix(5)
        return lastFiveItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let lastFiveItems = reversedArray.prefix(5)
        
        let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(WarehouseCollectionCell.self)",
                                                          for: indexPath) as? WarehouseCollectionCell        

        itemCell?.count.text = String(lastFiveItems[indexPath.row].count) + " шт"
        itemCell?.name.text = lastFiveItems[indexPath.row].nameOfItem
        
        switch  reversedArray[indexPath.row].sourceImage {
        case "gallery":
            itemCell?.imageView.image = UIImage(named:reversedArray[indexPath.row].imageURL ?? "emptyImage")
        case "photo":
            if let url = URL(string: reversedArray[indexPath.row].imageURL ?? "nil") {
                do {
                    let data = try Data(contentsOf: url)
                    itemCell?.imageView.image = UIImage(data: data)
                } catch {
                    print("Error = ", error.localizedDescription)
                }
            }
        default:
            itemCell?.imageView.image = UIImage(named: "Nothing")
        }

        return itemCell!
    }
    
    func setupFlowLayout()  -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        layout.sectionInset = .init(top: 0, left: 10, bottom: 0, right: 10)
        return layout
    }
    //MARK: Configure of all elements on VIEW
    func configure() {
        grabImageOnTop.image = UIImage(named: "FlatTriangle")
        grabImageOnTop.contentMode = .scaleToFill
                
        imageOfItem.image = UIImage(named: "Nothing")
        imageOfItem.backgroundColor = .clear
        imageOfItem.contentMode = .scaleAspectFill
        imageOfItem.clipsToBounds = true
        imageOfItem.layer.cornerRadius = 8
        imageOfItem.layer.borderWidth = 3
        imageOfItem.layer.borderColor = UIColor.white.cgColor
        
        sideButtonSet(button: photoButton, image: "camera")
        photoButton.addTarget(self, action: #selector(callTheCamera), for: .touchUpInside)
        
        sideButtonSet(button: selfGalleryButton, image: "gallery")
        selfGalleryButton.addTarget(self, action: #selector(generalGallery), for: .touchUpInside)
        
        addButton.setTitle("Add", for: .normal)
        addButton.backgroundColor = .customDarkPink
        addButton.layer.cornerRadius = 10
        addButton.titleLabel?.font = .systemFont(ofSize: 25)
        addButton.addTarget(self, action:#selector(tryToAddNewItem), for: .touchUpInside)
        
        textFieldsSet(textField: nameOfItem, text: "Item's name")
        textFieldsSet(textField: countOfItem, text: "Amount")
        
        countOfItem.keyboardType = .asciiCapableNumberPad
        
        if reversedArray.isEmpty {
            lastAddedLabel.isHidden = true
        }
        lastAddedLabel.text = "last added items"
        lastAddedLabel.font = .systemFont(ofSize: 20)
        lastAddedLabel.textAlignment = .center
    }
}

extension AddItemSheetViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate  {
  //MARK: CAMERA METHODS FOR  IMAGE
    @objc func callTheCamera() {
        let vc = UIImagePickerController()
        vc.delegate = self
        vc.allowsEditing = true
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            vc.sourceType = .camera
            present(vc, animated: true)
        } else {
            let alertControl = UIAlertController(title: nil, message: "Camera is not allowed", preferredStyle: .actionSheet)
            
            alertControl.view.backgroundColor = .systemRed
            alertControl.view.layer.cornerRadius = 22
            
            present(alertControl, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                alertControl.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage {
            imageOfItem.image = image
            TypeOfGallerySource.TOGS.state = .photo
            picker.dismiss(animated: true, completion: nil)
        }
    }

    //MARK: IMAGE SET
    func setImage(name:String) {
        imageOfItem.image = UIImage(named: name)
    }
    
    @objc func generalGallery() {
        let newVC = GeneralGallery()
        newVC.setImage = self

        if let sheet = newVC.sheetPresentationController {
                 sheet.detents = [.medium()]
                 sheet.prefersScrollingExpandsWhenScrolledToEdge = false
                 sheet.prefersGrabberVisible = false
                 sheet.preferredCornerRadius = 22
             }
             present(newVC, animated: true, completion: nil)
    }

        //MARK: TEXTFIELD METHODS
    @objc func tryToAddNewItem() {
        let alertText = CheckFields().whatMessageShowAndSave(nameField: nameOfItem.text!,
                                                              countField: countOfItem.text!)
        let intCount = Int(countOfItem.text!)
        
        if alertText.1 == .green {
            
            SaveMethods().saveItemWithPhoto(image: imageOfItem.image!,
                                            imageName: nameOfItem.text!,
                                            name: nameOfItem.text!,
                                            count: intCount!,
                                            sourceImage: TypeOfGallerySource.TOGS.state.rawValue)
            reloadDelegate?.reloadCollectionView()
        }
        
        clearAllFields()
        TypeOfGallerySource.TOGS.state = .none
        lastAddedItemCollection.reloadData()

        lastAddedItemCollection.isHidden = false
        
        let alertControl = UIAlertController(title: nil, message: alertText.0, preferredStyle: .actionSheet)
        
        alertControl.view.backgroundColor = alertText.1
        alertControl.view.layer.cornerRadius = 22

        present(alertControl, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            alertControl.dismiss(animated: true, completion: nil)
        }
            }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
        
    func clearAllFields() {
        nameOfItem.text = nil
        countOfItem.text = nil
        imageOfItem.image = UIImage(named: "Nothing")
        self.view.endEditing(true)
    }
}

extension AddItemSheetViewController {
    
    func textFieldsSet(textField: UITextField, text: String) {
        textField.placeholder = text
        textField.textAlignment = .center
        textField.font = .systemFont(ofSize: 20)
        textField.layer.borderColor = UIColor.gray.cgColor
        textField.layer.borderWidth = 2
        textField.layer.cornerRadius = 15
        textField.delegate = self
    }
    
    func sideButtonSet(button: UIButton, image name:String) {
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        button.backgroundColor = .customDarkPink
        button.setImage(UIImage(named: name), for: .normal)
        button.layer.cornerRadius = 22
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.white.cgColor
    }

    //MARK: Constraint
        func constraints() {
            let width = view.frame.width / 2
            NSLayoutConstraint.activate([
                grabImageOnTop.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
                grabImageOnTop.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                grabImageOnTop.widthAnchor.constraint(equalToConstant: view.frame.width / 3),
                grabImageOnTop.heightAnchor.constraint(equalToConstant: 30),
                
                imageOfItem.topAnchor.constraint(equalTo: grabImageOnTop.bottomAnchor, constant: 40),
                imageOfItem.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                imageOfItem.widthAnchor.constraint(equalToConstant: 200),
                imageOfItem.heightAnchor.constraint(equalToConstant: 200),
                
                nameOfItem.topAnchor.constraint(equalTo: imageOfItem.bottomAnchor, constant: 40),
                nameOfItem.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                nameOfItem.heightAnchor.constraint(equalToConstant: 50),
                nameOfItem.widthAnchor.constraint(equalToConstant: width),
                
                countOfItem.topAnchor.constraint(equalTo: nameOfItem.bottomAnchor, constant: 20),
                countOfItem.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                countOfItem.heightAnchor.constraint(equalToConstant: 50),
                countOfItem.widthAnchor.constraint(equalToConstant: width),
                
                photoButton.topAnchor.constraint(equalTo: imageOfItem.topAnchor, constant: 25),
                photoButton.leadingAnchor.constraint(equalTo: imageOfItem.trailingAnchor, constant: 20),
                photoButton.heightAnchor.constraint(equalToConstant: 50),
                photoButton.widthAnchor.constraint(equalToConstant: 50),
                
                selfGalleryButton.topAnchor.constraint(equalTo: imageOfItem.topAnchor, constant: 25),
                selfGalleryButton.trailingAnchor.constraint(equalTo: imageOfItem.leadingAnchor, constant: -25),
                selfGalleryButton.heightAnchor.constraint(equalToConstant: 50),
                selfGalleryButton.widthAnchor.constraint(equalToConstant: 50),
                
                addButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                addButton.topAnchor.constraint(equalTo: countOfItem.bottomAnchor, constant: 30),
                addButton.widthAnchor.constraint(equalToConstant: width),
                addButton.heightAnchor.constraint(equalToConstant: 50),
                
                lastAddedLabel.topAnchor.constraint(equalTo: addButton.bottomAnchor, constant: 25),
                lastAddedLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                lastAddedLabel.heightAnchor.constraint(equalToConstant: 30),
                lastAddedLabel.widthAnchor.constraint(equalToConstant: width),
            ])
        }
}
