//
//  WarehouseCollectionCell.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 24.01.2023.
//Сделать универсальной - Только картинка и картинка с текстом
import UIKit

final class WarehouseCollectionCell: UICollectionViewCell {
    var imageView = UIImageView()
    var name = UILabel()
    var count = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.backgroundColor = .customDarkPink
        contentView.layer.cornerRadius = 12
        contentView.layer.borderColor = UIColor.white.cgColor
        contentView.layer.borderWidth = 2

        setupImage()
        setupName()
        setupCount()
        constraint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupImage() {
        contentView.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor.black.cgColor
        imageView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        imageView.layer.cornerRadius = 8
        imageView.clipsToBounds = true
    }
    
    func setupName() {
        contentView.addSubview(name)
        name.translatesAutoresizingMaskIntoConstraints = false
        name.backgroundColor = .clear
        name.numberOfLines = 1
        name.textAlignment = .center
        name.font = UIFont.boldSystemFont(ofSize: 16.0)
        name.layer.borderColor = UIColor.black.cgColor
        name.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        name.layer.borderWidth = 2
        name.layer.cornerRadius = 8
    }
    
    func setupCount() {
        contentView.addSubview(count)
        count.translatesAutoresizingMaskIntoConstraints = false
        count.backgroundColor = .clear
        count.numberOfLines = 1
        count.font = UIFont.boldSystemFont(ofSize: 20.0)
        count.textAlignment = .center
    }
    
    func constraint() {
        let padding = 8.0
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: padding),
            imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: padding),
            imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -padding),
            imageView.heightAnchor.constraint(equalToConstant: 100),
            imageView.widthAnchor.constraint(equalToConstant: 100),
            
            name.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -4),
            name.leadingAnchor.constraint(equalTo: imageView.leadingAnchor),
            name.trailingAnchor.constraint(equalTo: imageView.trailingAnchor),
            name.heightAnchor.constraint(equalToConstant: 30),

            count.topAnchor.constraint(equalTo: name.bottomAnchor, constant: 5),
            count.leadingAnchor.constraint(equalTo: imageView.leadingAnchor),
            count.trailingAnchor.constraint(equalTo: imageView.trailingAnchor),
            count.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -padding)
        ])
    }
}
