//
//  WeatherMainView.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 14.03.2023.
//

import UIKit

final class WeatherMainView: UIViewController,
                             UITableViewDelegate,
                             UITableViewDataSource {
    
    let currentLocation = UILabel() // Show city
    let weatherDescription = UILabel() // Cloudy or sunny for example
    let weatherCard = UIView()
    
    let weatherTableView: UITableView = {
          let tableView = UITableView()
          tableView.register(WeatherCell.self, forCellReuseIdentifier: WeatherCell.identifierCell)
          return tableView
      }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .customLightPink
        backBarButton()
        
        cityLabelConfig()
        weatherDay()
        tableViewSet()
    }
}
 
extension WeatherMainView {
    
    func tableViewSet() {
        view.addSubview(weatherTableView)
        weatherTableView.translatesAutoresizingMaskIntoConstraints = false
        
        weatherTableView.dataSource = self
        weatherTableView.delegate = self
        weatherTableView.backgroundColor = .clear
        weatherTableView.separatorColor = .white
        weatherTableView.allowsSelection = false
        weatherTableView.separatorStyle = .singleLine
        weatherTableView.separatorInset = .init(top: 0, left: 100, bottom: 0, right: 100)
        
        NSLayoutConstraint.activate([
            weatherTableView.topAnchor.constraint(equalTo: weatherCard.bottomAnchor, constant: 10),
            weatherTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10),
            weatherTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            weatherTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10)
        ])
    }
        
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        WeatherModel.weather.daysArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: WeatherCell.identifierCell) as! WeatherCell

        let dayOfWeek = Date(timeIntervalSince1970: Double(WeatherModel.weather.daysArray[indexPath.row].date_ts)).toString
                        
        cell.image.image = UIImage(named: "\(WeatherModel.weather.daysArray[indexPath.row].parts.day.condition)")
        cell.temp.text = "\(Int(WeatherModel.weather.daysArray[indexPath.row].parts.day.temp_avg)) C°"
        cell.dayLabel.text = dayOfWeek
        cell.condition.text = "\(WeatherModel.weather.daysArray[indexPath.row].parts.day.condition)"
        cell.wind.text =  "wind \(Int(WeatherModel.weather.daysArray[indexPath.row].parts.day.wind_speed))m/s"
        cell.windSpeed = WeatherModel.weather.daysArray[indexPath.row].parts.day.wind_speed
        cell.windGust.text =  " gust \(Int(WeatherModel.weather.daysArray[indexPath.row].parts.day.wind_gust))m/s"

        return cell
    }
        
    //MARK: weatherView config                           ------------------------------------
    private func weatherDay() {
        let imageOfCloudness = UIImageView()
        let dayOfWeek = UILabel()
        let cloudness = UILabel()
        let wind = UILabel()
        let windGust = UILabel()
        let temperature = UILabel()
        
        view.addSubview(weatherCard)
        weatherCard.translatesAutoresizingMaskIntoConstraints = false
        
        let array = [imageOfCloudness, dayOfWeek, cloudness, wind, temperature, windGust]
        for item in array {
            item.translatesAutoresizingMaskIntoConstraints = false
            weatherCard.addSubview(item)
        }
        
        weatherCard.backgroundColor = .clear
        weatherCard.layer.cornerRadius = 10
        weatherCard.layer.borderWidth = 4
        weatherCard.clipsToBounds = true
    
        if let wind = WeatherModel.weather.windSpeed {
            switch wind {
            case 0.0...7.9 :
                weatherCard.layer.borderColor = UIColor.green.cgColor
            case 8.0...10.7:
                weatherCard.layer.borderColor = UIColor.yellow.cgColor
            case 10.8...:
                weatherCard.layer.borderColor = UIColor.red.cgColor
            default:
                weatherCard.layer.borderColor = UIColor.white.cgColor
            }
        } else {
            weatherCard.layer.borderColor = UIColor.white.cgColor
        }

        imageOfCloudness.image = UIImage(named: "\(WeatherModel.weather.overcast!)")
        imageOfCloudness.contentMode = .scaleAspectFit
        
        let todayDate = Date(timeIntervalSince1970: Double(WeatherModel.weather.dayToday)).toString

        customLabel(label: dayOfWeek, text: "\(todayDate)")
        customLabel(label: cloudness, text: WeatherModel.weather.overcast!)
        customLabel(label: wind, text: "Wind \(Int(WeatherModel.weather.windSpeed!)) m/sec)")
        customLabel(label: windGust, text: "Gust \(WeatherModel.weather.windGust!) m/sec")
        customLabel(label: temperature, text: "\(WeatherModel.weather.temp!) C°")
        
        NSLayoutConstraint.activate([
            dayOfWeek.topAnchor.constraint(equalTo: weatherCard.topAnchor, constant: 10),
            dayOfWeek.centerXAnchor.constraint(equalTo: weatherCard.centerXAnchor),
            dayOfWeek.heightAnchor.constraint(equalToConstant: 30),
            dayOfWeek.widthAnchor.constraint(equalToConstant: 130),
            
            cloudness.topAnchor.constraint(equalTo: dayOfWeek.bottomAnchor, constant: 10),
            cloudness.leadingAnchor.constraint(equalTo: imageOfCloudness.trailingAnchor, constant: 10),
            cloudness.trailingAnchor.constraint(equalTo: weatherCard.trailingAnchor, constant: -10),
            cloudness.heightAnchor.constraint(equalToConstant: 30),
            
            temperature.topAnchor.constraint(equalTo: cloudness.bottomAnchor, constant: 5),
            temperature.leadingAnchor.constraint(equalTo: imageOfCloudness.trailingAnchor, constant: 10),
            temperature.trailingAnchor.constraint(equalTo: weatherCard.trailingAnchor, constant: -10),
            temperature.heightAnchor.constraint(equalToConstant: 30),
            
            wind.topAnchor.constraint(equalTo: temperature.bottomAnchor, constant: 5),
            wind.leadingAnchor.constraint(equalTo: imageOfCloudness.trailingAnchor, constant: 10),
            wind.trailingAnchor.constraint(equalTo: weatherCard.trailingAnchor, constant: -10),
            wind.heightAnchor.constraint(equalToConstant: 30),
            
            windGust.topAnchor.constraint(equalTo: wind.bottomAnchor, constant: 5),
            windGust.leadingAnchor.constraint(equalTo: imageOfCloudness.trailingAnchor, constant: 10),
            windGust.trailingAnchor.constraint(equalTo: weatherCard.trailingAnchor, constant: -10),
            windGust.heightAnchor.constraint(equalToConstant: 30),

                        
            imageOfCloudness.topAnchor.constraint(equalTo: dayOfWeek.bottomAnchor, constant: 10),
            imageOfCloudness.bottomAnchor.constraint(equalTo: weatherCard.bottomAnchor, constant: -10),
            imageOfCloudness.leadingAnchor.constraint(equalTo: weatherCard.leadingAnchor, constant: 10),
            imageOfCloudness.trailingAnchor.constraint(equalTo: weatherCard.trailingAnchor, constant: -(view.frame.width/2 - 10)),
            
            weatherCard.topAnchor.constraint(equalTo: currentLocation.bottomAnchor, constant: 10),
            weatherCard.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            weatherCard.heightAnchor.constraint(equalToConstant: 200),
            weatherCard.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            weatherCard.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
        ])
    }
    
  //MARK: weatherlabel config                         ------------------------------------
    private func cityLabelConfig() {
        customLabel(label: currentLocation, text: WeatherModel.weather.city ?? "KGD")
        
        NSLayoutConstraint.activate([
            currentLocation.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
            currentLocation.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            currentLocation.heightAnchor.constraint(equalToConstant: 40),
            currentLocation.widthAnchor.constraint(equalToConstant: 200),
        ])
    }

    //MARK: other useful methods                        ------------------------------------
    private func customLabel(label: UILabel, text: String) {
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false

        label.text = text
        label.textAlignment = .center
        label.backgroundColor = .customCarmin

        label.layer.cornerRadius = 10
        label.layer.borderColor = UIColor.white.cgColor
        label.layer.borderWidth = 2
        label.clipsToBounds = true
    }

    private func backBarButton() {
        let button = UIButton(type: .custom)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        button.backgroundColor = .clear
        button.setImage(UIImage(named: "backIcon"), for: .normal)
        button.addTarget(self, action: #selector(back), for: UIControl.Event.touchUpInside)

        let menuBarItem = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = menuBarItem
    }
    
    @objc func back() {
       navigationController?.popViewController(animated: true)
   }
}
