//
//  WeatherCell.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 27.03.2023.
//

import UIKit

class WeatherCell: UITableViewCell {
    
    static let identifierCell = "identifierCell"
    
    let dayLabel = UILabel()
    let image = UIImageView()
    let temp = UILabel()
    let wind = UILabel()
    let condition = UILabel()
    let windGust = UILabel()
    let indicator = UILabel()
            
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = .customLightPink
        
        let showOnVC = [dayLabel, image, temp, wind, condition, windGust, indicator ]
        
        for i in showOnVC {
            contentView.addSubview(i)
            i.translatesAutoresizingMaskIntoConstraints = false
        }
        
        constraints()
        customElement()
    }
    
    var windSpeed = 0.0
    
    private func customElement() {
        textStandarts(label: dayLabel)
        textStandarts(label: temp)
        textStandarts(label: wind)
        textStandarts(label: windGust)
        textStandarts(label: condition)
        
        dayLabel.textColor = .customCarmin
        image.contentMode = .scaleAspectFit
        image.backgroundColor = .clear
        
        whatWindColor(wind: windSpeed)
    }
    
   private func whatWindColor(wind:Double) {
        indicator.layer.borderWidth = 4

        switch wind {
        case 0.0...7.9 :
            indicator.layer.borderColor = UIColor.green.cgColor
        case 8.0...10.7:
            indicator.layer.borderColor = UIColor.yellow.cgColor
        case 10.8...:
            indicator.layer.borderColor = UIColor.red.cgColor
        default:
            indicator.layer.borderColor = UIColor.white.cgColor
        }
    }
        
    private func constraints(){
        let margin = 5.0
        NSLayoutConstraint.activate([
            indicator.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            indicator.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            indicator.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            indicator.widthAnchor.constraint(equalToConstant: 5),
            
            dayLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: margin),
            dayLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            dayLabel.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: margin),
          //  dayLabel.widthAnchor.constraint(equalToConstant: contentView.frame.width / 3),

            image.topAnchor.constraint(equalTo: contentView.topAnchor, constant: margin),
            image.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: margin + 15),
            image.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -margin),
            image.heightAnchor.constraint(equalToConstant: 70),
            image.widthAnchor.constraint(equalToConstant: 70),
            
            temp.topAnchor.constraint(equalTo: dayLabel.bottomAnchor, constant: margin),
            temp.widthAnchor.constraint(equalToConstant: contentView.frame.width / 3 - 10),
            temp.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),

            
            condition.topAnchor.constraint(equalTo: temp.bottomAnchor, constant: margin),
            condition.widthAnchor.constraint(equalToConstant: contentView.frame.width - image.frame.width - windGust.frame.width),
            condition.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            
            wind.topAnchor.constraint(equalTo: temp.topAnchor, constant: 0),
            wind.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -margin),
            wind.leadingAnchor.constraint(equalTo: temp.trailingAnchor, constant: margin),
            
            windGust.topAnchor.constraint(equalTo: condition.topAnchor, constant: 0),
            windGust.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -margin),
            windGust.leadingAnchor.constraint(equalTo: temp.trailingAnchor, constant: margin),
            ])
        }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension WeatherCell {
    private func textStandarts(label: UILabel) {
        label.textAlignment = .center
        label.backgroundColor = .clear
    }
}
