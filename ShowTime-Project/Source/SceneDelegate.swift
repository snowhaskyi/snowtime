//
//  SceneDelegate.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 22.01.2023.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        if let WC = scene as? UIWindowScene {
            
            let window = UIWindow(windowScene: WC)
            let navigationController = UINavigationController()
            let VC = MainView() // mainStartView
           // let VC = WarehouseCollection() //delete after in the end
            
            navigationController.viewControllers = [VC]
            window.rootViewController = navigationController
            
            self.window = window
            
            window.makeKeyAndVisible()
        }
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    func sceneDidDisconnect(_ scene: UIScene) {

    }

    func sceneDidBecomeActive(_ scene: UIScene) {

    }

    func sceneWillResignActive(_ scene: UIScene) {

    }

    func sceneWillEnterForeground(_ scene: UIScene) {

    }

    func sceneDidEnterBackground(_ scene: UIScene) {

    }


}

