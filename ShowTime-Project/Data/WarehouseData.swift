//  Created by Wi-Fai on 22.01.2023.


import UIKit

//Модель временного хранения данных для будущей записи данных
final class ModelOfItemForSave: Codable  {
    var nameOfItem:String
    var count:Int
    var imageURL:String?
    var sourceImage: String
    
    init(nameOfItem: String, count: Int, imageUrl:String, sourceImage: String ) {
        self.nameOfItem = nameOfItem
        self.count = count
        self.imageURL = imageUrl
        self.sourceImage = sourceImage
    }
}

final class WarehouseItems {
     static var warehouseItem = WarehouseItems()
    var itemsArray:[ModelOfItemForSave] {RestoreItemsFromFile().getItems() ?? [] }
 }

