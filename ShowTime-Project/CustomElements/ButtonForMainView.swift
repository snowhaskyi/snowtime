//
//  ButtonForMainView.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 22.01.2023.
//

import UIKit

struct CustomButtonViewModel {
    let title:String
    let imageName:String
}

final class ButtonForMainView: UIButton {
     
     private var viewModel: CustomButtonViewModel?

     private let newTitleLabel: UILabel = {
         let label = UILabel()
         label.numberOfLines = 1
         label.font = UIFont.boldSystemFont(ofSize: 20.0)
         label.textAlignment = .center
         return label
     }()
    
    private let iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
          
     override init(frame:CGRect) {
         self.viewModel = nil
         super.init(frame: .zero)
     }

     init(with viewModel:CustomButtonViewModel) {
         self.viewModel = viewModel
         super.init(frame: .zero)
         
         configure(with: viewModel)
     }
        
     override func layoutSubviews() {
         super.layoutSubviews()
         iconView.frame = CGRect(x: 10, y: 5, width: 50, height: frame.height - 10).integral
         newTitleLabel.frame = CGRect(x: 32, y: 0, width: frame.width - 55, height: frame.height).integral
     }
     
     required init?(coder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
     }
     
     public func configure(with viewModel: CustomButtonViewModel) {
         backgroundColor = .customVioletDark
         layer.cornerRadius = 10
         layer.borderColor = UIColor.white.cgColor
         layer.borderWidth = 2
         layer.shadowColor = UIColor.white.cgColor
         layer.shadowRadius = 7
         layer.shadowOpacity = 0.4
         layer.shadowOffset = CGSize(width: 5, height: 5)
         
         addSubview(newTitleLabel)
         addSubview(iconView)
         newTitleLabel.text = viewModel.title
         iconView.image = UIImage(named: viewModel.imageName)
     }
}
