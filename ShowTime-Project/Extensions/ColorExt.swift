//
//  ColorExt.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 23.01.2023.
//

import UIKit
extension UIColor {
    static let customVioletDark = UIColor(red: 0.27, green: 0.23, blue: 0.31, alpha: 1.00)
    static let customLightPink = UIColor(red: 0.89, green: 0.76, blue: 0.83, alpha: 1.00)
    static let customDarkPink = UIColor(red: 0.50, green: 0.35, blue: 0.45, alpha: 1.00)
    static let customCarmin = UIColor(red: 0.81, green: 0.48, blue: 0.53, alpha: 1.00)
    static let customCurrant = UIColor(red: 0.28, green: 0.25, blue: 0.40, alpha: 1.00)
    
    /// почти бежевый
    static let workColorNavajoWhite = UIColor(red: 0.98, green: 0.78, blue: 0.64, alpha: 1.00)
    /// почти розовый
    static let workColorLightCoral = UIColor(red: 0.96, green: 0.66, blue: 0.66, alpha: 1.00)
    /// светло-фиолетовый
    static let workColorRosyBrown = UIColor(red: 0.81, green: 0.59, blue: 0.69, alpha: 1.00)
    /// светло-сиреневый
    static let workColorLightStateGray = UIColor(red: 0.69, green: 0.61, blue: 0.72, alpha: 1.00)
    static let workColorLightBlue = UIColor(red: 0.52, green: 0.75, blue: 0.85, alpha: 1.00)
}
