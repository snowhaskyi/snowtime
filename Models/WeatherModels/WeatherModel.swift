//
//  WeatherModel.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 22.03.2023.
//

import UIKit

class WeatherModel {
    static var weather = WeatherModel()
    
    var daysArray: [Forecasts] = []
    var dayToday = 0
    
    var city: String?
    var windSpeed: Double?
    var windGust: Double?
    var overcast: String?
    var temp: Int?
    
    init(city: String? = nil, windSpeed: Double? = nil, windGust: Double? = nil, overcast: String? = nil, temp: Int? = nil) {
        self.city = city
        self.windSpeed = windSpeed
        self.windGust = windGust
        self.overcast = overcast
        self.temp = temp
    }
}
