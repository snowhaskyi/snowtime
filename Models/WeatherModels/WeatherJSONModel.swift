//
//  WeatherJSONModel.swift
//  ShowTime-Project
//
//  Created by Wi-Fai on 18.03.2023.
//

import Foundation

struct WeatherJSON: Decodable {
    let now: Int
    let info: Info
    let geo_object: GeoObject
    let fact: Fact
    var forecasts: [Forecasts]
    
}

struct Info: Decodable {
    let tzinfo: Descript
}

struct GeoObject: Decodable {
    let locality: Locality
}

struct Locality: Decodable {
    let id: Int
    let name: String
}

struct Descript: Decodable {
    let name: String
}

struct Forecasts: Decodable {
    let date: String
    let date_ts: Double 
    let week: Int
    let parts: Parts
}

struct Parts: Decodable {
    let day: Day
}

struct Day: Decodable {
    let temp_avg: Double // не уверен, но кажется это средняя по дню
    let wind_speed: Double
    let wind_gust: Double
    let condition: String //дождь снег или что там
}

struct Fact: Decodable {
    let temp: Int
    let condition: String
    let wind_speed: Double
    let wind_gust: Double //порывы ветра
    let cloudness: Double
}
